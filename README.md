# Trendyol Seller Center UI Tests


## Setup

```
install docker
```

## Launch

**Test application:**
```
make browser=ff test-selenium
make browser=gc test-selenium
```

**Test Infra:**
```
docker-compose up -d hub firefox chrome
```

**Robot Framework Tests with browsers:**
```
docker-compose run test-gc
docker-compose run test-ff
------ or ------
docker-compose up test-gc test-ff

```

**Reports:**  
After test run,test output report files will appear in [results](results) ff and gc folders.


**View & Debug Execution:**  
Connect with vnc to the browsers (in 7900). When prompted for password the default is `secret`.

## Cleanup
```
make clean-test-selenium
```
